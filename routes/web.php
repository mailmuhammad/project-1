<?php


Auth::routes();

Route::group(['middleware' => ['auth']], function () {

Route::get('/', 'HomeController@index');

Route::group(['middleware' => 'admin'], function() {
	Route::resources([
		'categories' => 'Product_CategoriesController',
		'member_categories' => 'Member_CategoriesController',
		
	]);
});

Route::resources([
	'products' => 'ProductController',
	'promos' => 'PromoController',
	'transactions' => 'TransactionsController',
	'members' => 'MembersController',
]);

// Route::get('/home', 'HomeController@index')->name('home');
    
});

