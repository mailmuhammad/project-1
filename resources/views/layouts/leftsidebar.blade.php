<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="{{ asset('assets/images/user.png') }}" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                <div class="email">{{ Auth::user()->email }}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('logout') }}" 
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"
                            ><i class="material-icons">input</i>Sign Out</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active">
                    <a href="/">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>    
                <li>
                    <a href="{{ route('products.index') }}">
                        <i class="material-icons">assessment</i>
                        <span>products</span>
                    </a>
                </li>
                @if(Auth::user()->role == 'admin')
                    <li>
                        <a href="{{ route('categories.index') }}">
                            <i class="material-icons">assignment</i>
                            <span>product_categories</span>
                        </a>
                    </li>
                    <li>
                @endif

                    <a href="{{ route('promos.index') }}">
                        <i class="material-icons">receipt</i>
                        <span>Promo</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('transactions.index') }}">
                        <i class="material-icons">payment</i>
                        <span>Transactions</span>
                    </a>
                </li>
                @if(Auth::user()->role == 'admin')
                <li>
                    <a href="{{ route('member_categories.index') }}">
                        <i class="material-icons">assignment_ind</i>
                        <span>Member Category</span>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ route('members.index') }}">
                        <i class="material-icons">account_box</i>
                        <span>Member</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2019 <a href="javascript:void(0);"></a>
            </div>
            <div class="version">
                <b>Version: </b> 1.0.0
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>