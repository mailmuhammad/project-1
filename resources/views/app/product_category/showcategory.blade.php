@extends('layouts.home')
@section('title', $category->name )
@section('content')
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                        	<div class="table-resposive">
								<a href="/categories"><h1>{{ $category->name }}</h1></a>
								<p>{{ $category->desc }}</p>
								<hr>
								<ol style="margin-left: -20px">
								@foreach ($category->prod as $product)
									<p>{{ $product['name'] }}</p>
								@endforeach
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>
</section>
@endsection