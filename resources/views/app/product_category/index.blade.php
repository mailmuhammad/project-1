@extends('layouts.home')
@section('title', 'Admin BSB | Product Category')
@section('content')

<body class="theme-red">
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <button type="button" class="btn bg-purple waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Add New Product Category</button>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <p align="center">Product Category</p>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if($categories->count())
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Update</th>
                                            <th>Show</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <ol>
                                            @foreach ($categories as $c => $category)
                                                <tr>
                                                    <td>{{ $c+1 }}</td>
                                                    <td>{{ $category->name }}</td>
                                                    <td>{{ $category->desc }}</td>
                                                    <td><a href="{{ route('categories.edit', $category->id) }}"><button type="submit" class="btn bg-purple">Edit</button></a></td>
                                                    <td><a href="{{ route('categories.show', $category->id) }}"><button type="submit" class="btn bg-blue">Show</button></a></td>
                                                    <form action="{{ route('categories.destroy', $category->id) }}" id="deletedata-{{ $category->id }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <td><button type="submit" class="btn bg-red" onclick="deleteNih({{ $category->id }})">Delete</button></td>
                                                    </form>
                                                </tr>   
                                            @endforeach
                                        </ol>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @else
                            <div class="alert alert-info">You don't have any category</div>
                        @endif
	                    <!-- Modal Dialogs ====================================================================================================================== -->
	                    <!-- Default Size -->
	                    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
	                        <div class="modal-dialog" role="document">
	                            <div class="modal-content">
	                                <div class="modal-header">
	                                    <h4 class="modal-title" id="defaultModalLabel" align="center" >Input Category Product</h4>
	                                </div>
	                                <div class="body">
	                                    <form action="{{ route('categories.store') }}" method="post" id="simpanData" autocomplete="off" class="form-group">
	                                        @csrf
	                                        <div class="form-line">
	                                            <label for="title">Name</label>
	                                            <input type="text" id="name" name="name" class="form-control">
	                                            
	                                        </div>

	                                        <div class="form-line"><br>
	                                            <label for="description">Description</label>
	                                            <textarea id="desc" name="desc" rows="3" class="form-control"></textarea>    

	                                        </div>
	                                        
	                                        <br><div class="modal-footer"><br>
	                                            <button type="submit" class="btn btn-link waves-effect">SAVE</button>
	                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
	                                        </div>

	                                    </form>  
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                </div>
            </div>
           
        </div>
    </section>
</body>

@endsection
@section('script')

<script type="text/javascript" class="init">
    function deleteNih(id){
        event.preventDefault();
        var form = document.querySelector('#deletedata-'+id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            form.submit();
        })
    }

    $(document).ready(function() {
        $('#simpanData').on('submit',function(e){
            e.preventDefault();
            // console.log('uoioiu');

            var form = document.querySelector('#simpanData');
            var data = new FormData(form);

            swal({
                title: 'Success',
                text:"Berhasil Menambahkan Data",
                type:"success",
                timer: 1000,
                showConfirmButton: false
            }, function() {
                form.submit();
            });
        })
    });

</script>
@endsection