@extends('layouts.home')
@section('title', 'Admin BSB | Promo')
@section('content')

<body class="theme-red">
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <button type="button" class="btn bg-purple waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Add New Promo</button>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <p align="center">Promo</p>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if($promos->count())
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Promo</th>
                                            <th>Produk</th>
                                            <th>Discount</th>
                                            <th>Edit</th>
                                            <th>Hapus</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Promo</th>
                                            <th>Produk</th>
                                            <th>Discount</th>
                                            <th>Edit</th>
                                            <th>Hapus</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <ol>
                                            @foreach ($promos as $p => $promo)
                                                <tr>
                                                    <td>{{ $p+1 }}</td>
                                                    <td>{{ $promo->name }}</td>
                                                    <td>{{ $promo->produc['name'] }}</td>
                                                    <td>{{ $promo->discount }}</td>
                                                    <td><a href="/promos/{{ $promo->id }}/edit"><button type="submit" class="btn bg-purple">Edit</button></a></td>
                                                    <form action="{{ route('promos.destroy', $promo->id) }}" id="deletedata-{{ $promo->id }}" method="post">
                                                        @csrf 
                                                        @method('delete')
                                                        <td><button type="submit" class="btn bg-red" onclick="deleteNih({{ $promo->id }})">Delete</button></td>
                                                    </form>
                                                </tr>   
                                            @endforeach
                                        </ol>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @else
                            <div class="alert alert-info">You don't have any Promo</div>
                        @endif
	                    <!-- Modal Dialogs ====================================================================================================================== -->
	                    <!-- Default Size -->
	                    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
	                        <div class="modal-dialog" role="document">
	                            <div class="modal-content">
	                                <div class="modal-header">
	                                    <h4 class="modal-title" id="defaultModalLabel" align="center" >Input Promo</h4>
	                                </div>
	                                <div class="body">
	                                    <form action="{{ route('promos.store') }}" method="post" id="simpanData" autocomplete="off" class="form-group">
	                                        @csrf
                                            <div class="form-group">
	                                           <div class="form-inline">
                                                    <div class="form-group mb-2">
	                                                   <label for="name">Kode Promo</label>
                                                    </div>
                                                    <div class="form-group mx-sm-3 mb-2">
	                                                   <input type="text" id="name" name="name" class="form-control" required>    
	                                                </div>
                                                    <button type="button" class="btn btn-primary mb-2" onclick="codeButton()">Acak</button>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="produxt_id">Promo</label>
                                                    <div class="form-line">
                                                        <select  class="form-control" name="product_id" id="product_id">
                                                        <option disabled selected>-- Pilih Satu --</option>
                                                            @foreach($produc as $pro)
                                                                <option value="{{ $pro->id }}"> {{ $pro->name }} </option>
                                                            @endforeach
                                                        </select> 
                                                    </div>
                                            </div>
                                            <div class="form-group">
	                                           <div class="form-line"><br>
	                                               <label for="discount">Discount</label>
	                                               <input type="text" id="discount" name="discount" class="form-control" required>    
	                                           </div>
                                            </div>
	                                        <br><div class="modal-footer"><br>
	                                            <button type="submit" class="btn btn-link waves-effect">SAVE</button>
	                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
	                                        </div>

	                                    </form>  
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                </div>
            </div>
           
        </div>
    </section>

@endsection
@section('script')

<script type="text/javascript" class="init">
    function deleteNih(id){
        event.preventDefault();
        var form = document.querySelector('#deletedata-'+id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            form.submit();
        })
    }

    $(document).ready(function() {
        $('#simpanData').on('submit',function(e){
            e.preventDefault();
            // console.log('uoioiu');

            var form = document.querySelector('#simpanData');
            var data = new FormData(form);
            // console.log(data);

            swal({
                title: 'Success',
                text:"Berhasil Menambahkan Data",
                type:"success",
                timer: 1000,
                showConfirmButton: false
            }, function() {
                form.submit();
            });
        })
    });

        function codeButton(){
            $('#name').val(Math.random().toString(36).substr(2, 5));
        }

</script>
@endsection