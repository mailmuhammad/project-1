@extends('layouts.home')

@section('title', 'Admin BSB | Edit Promo')

@section('content')

<section class="content">
<div class="container-fluid">
    <div class="block-header"></div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Promo</h2>
                </div>
                <div class="body">
                    <form action="{{ route('promos.update', $promo->id) }}" method="post" autocomplete="off" class="form-group" >
                        @csrf
                        @method('put')
                        <div class="form-line"><br>
                            <label for="name">Kode Promo</label>
                            <input type="text" id="name" name="name" class="form-control" value="{{ $promo->name }}">
                        </div>
                        <div class="form-line"><br>
                            <label>Kategori</label>
                            <select name="product_id" id="product_id" class="form-control">
                                <option disabled selected>Pilih Satu</option>
                                @foreach($product as $prod) 
                                    <option {{ $promo->product_id == $prod->id ? "selected" : "" }} value="{{ $prod->id }}">{{ $prod->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-line"><br>
                            <label for="discount">Discount</label>
                            <input id="discount" name="discount" class="form-control" value="{{ $promo->discount }}">
                        </div>
                        <br><button type="submit" class="btn bg-purple">Edit</button>
                        <a href="/promos"><button type="submit" class="btn bg-green" style="margin: 15px">Back</button></a>
                    </form>  
                </div>
            </div>
            </div>
        </div>
</div>
</div>
</section>
    

@endsection