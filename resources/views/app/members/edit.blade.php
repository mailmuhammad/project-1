@extends('layouts.home')

@section('title', 'Admin BSB | Edit Member')

@section('content')

<section class="content">
<div class="container-fluid">
    <div class="block-header"></div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Member</h2>
                </div>
                <div class="body">
                    <form action="{{ route('members.update', $member->id) }}" method="post" autocomplete="off" class="form-group" >
                        @csrf
                        @method('put')
                        <div class="form-line"><br>
                            <label>Kategori</label>
                            <select name="member_category_id" id="member_category_id" class="form-control">
                                <option disabled selected>Pilih Satu</option>
                                @foreach($member_categories as $cat) 
                                    <option {{ $member->member_category_id == $cat->id ? "selected" : "" }} value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-line"><br>
                            <label for="full_name">Nama Lengkap</label>
                            <input id="full_name" name="full_name" class="form-control" value="{{ $member->full_name }}">
                        </div>
                        <div class="form-line"><br>
                            <label for="dob">Tanggal Lahir</label>
                            <input id="dob" name="dob" type="date" class="form-control" value="{{ $member->dob }}">
                        </div>
                        <div class="form-line"><br>
                            <label for="address">Alamat</label>
                            <input id="address" name="address" class="form-control" value="{{ $member->address }}">
                        </div>
                        <div class="demo-radio-button">
                            <br><label>Jenis Kelamin</label>
                            <br><input type="radio" name="gender" id="laki-laki" {{ $member->gender == 'laki-laki' ? "checked" : "" }} value="laki-laki" class="with-gap">
                            <label for="laki-laki">Laki-laki</label>
                            <input type="radio" name="gender" id="perempuan" {{ $member->gender == 'perempuan' ? "checked" : "" }} value="perempuan" class="with-gap">
                            <label for="perempuan">Perempuan</label>
                        </div>
                        <div class="form-line"><br>
                            <label for="barcode">Barcode</label>
                            <input disabled id="barcode" name="barcode" class="form-control" value="{{ $member->barcode }}">
                        </div>
                        <br><button type="submit" class="btn bg-purple">Edit</button>
                        <a href="{{ route('members.index') }}"><button type="button" class="btn bg-green" style="margin: 15px">Back</button></a>
                    </form>  
                </div>
            </div>
            </div>
        </div>
</div>
</div>
</section>
    

@endsection