@extends('layouts.home')
@section('title', 'Admin BSB | Members')
@section('content')

<body class="theme-red">
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <button type="button" class="btn bg-purple waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Add New Member</button>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <p align="center">Member</p>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if($members->count())
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kategori</th>
                                            <th>Nama Lengkap</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Alamat</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Barcode</th>
                                            <th>Edit</th>
                                            <th>Hapus</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Kategori</th>
                                            <th>Nama Lengkap</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Alamat</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Barcode</th>
                                            <th>Edit</th>
                                            <th>Hapus</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <ol>
                                            @foreach ($members as $m => $member)
                                                <tr>
                                                    <td>{{ $m+1 }}</td>
                                                    <td>{{ $member->category['name'] }}</td>
                                                    <td>{{ $member->full_name }}</td>
                                                    <td>{{ $member->dob }}</td>
                                                    <td>{{ $member->address }}</td>
                                                    <td>{{ $member->gender }}</td>
                                                    <td>{{ $member->barcode }}</td>
                                                    <td><a href="{{ route('members.edit', $member->id) }}"><button type="submit" class="btn bg-purple">Edit</button></a></td>
                                                    <form action="{{ route('members.destroy', $member->id) }}" id="deletedata-{{ $member->id }}" method="post">
                                                        @csrf 
                                                        @method('delete')
                                                        <td><button type="submit" class="btn bg-red" onclick="deleteNih({{ $member->id }})">Delete</button></td>
                                                    </form>
                                                </tr>   
                                            @endforeach
                                        </ol>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @else
                            <div class="alert alert-info">You don't have any Member</div>
                        @endif
	                    <!-- Modal Dialogs ====================================================================================================================== -->
	                    <!-- Default Size -->
	                    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
	                        <div class="modal-dialog" role="document">
	                            <div class="modal-content">
	                                <div class="modal-header">
	                                    <h4 class="modal-title" id="defaultModalLabel" align="center" >Input Member</h4>
	                                </div>
	                                <div class="body">
	                                    <form action="{{ route('members.store') }}" method="post" id="simpanData" autocomplete="off" class="form-group">
	                                        @csrf
                                            <div class="form-group">
                                                <label for="member_category_id">Kategori</label>
                                                <div class="form-line">
                                                    <select  class="form-control" name="member_category_id" id="member_category_id" required>
                                                    <option disabled selected>-- Pilih Satu --</option>
                                                        @foreach($member_categories as $cat)
                                                            <option value="{{ $cat->id }}"> {{ $cat->name }} </option>
                                                        @endforeach
                                                    </select> 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            	<div class="form-line">
                                            		<label for="full_name">Nama Lengkap</label>
                                            		<input type="text" id="full_name" name="full_name" class="form-control" required>
                                            	</div>
                                            </div>
                                            <div class="form-group">
	                                           <div class="form-line"><br>
	                                               <label for="dob">Tanggal Lahir</label>
	                                               <input type="date" id="dob" name="dob" class="form-control" required>    
	                                           </div>
                                            </div>
                                            <div class="form-group">
                                            	<div class="form-line">
                                            		<label for="address">Alamat</label>
                                            		<input type="text" id="address" name="address" class="form-control" required>
                                            	</div>
                                            </div>
                                            <div class="form-group">
                                            	<div class="demo-radio-button">
                                            		<label>Jenis Kelamin</label><br><br>
                                            		<input type="radio" name="gender" id="laki-laki" value="laki-laki" class="with-gap">
                                            		<label for="laki-laki">Laki-laki</label>
                                            		<input type="radio" name="gender" id="perempuan" value="perempuan" class="with-gap">
                                            		<label for="perempuan">Perempuan</label>
                                            	</div>
                                            </div>
	                                        <br><div class="modal-footer"><br>
	                                            <button type="submit" class="btn btn-link waves-effect">SAVE</button>
	                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
	                                        </div>
	                                    </form>  
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                </div>
            </div>
           
        </div>
    </section>

@endsection
@section('script')

<script type="text/javascript" class="init">
    function deleteNih(id){
        event.preventDefault();
        var form = document.querySelector('#deletedata-'+id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            form.submit();
        })
    }

    $(document).ready(function() {
        $('#simpanData').on('submit',function(e){
            e.preventDefault();
            // console.log('uoioiu');

            var form = document.querySelector('#simpanData');
            var data = new FormData(form);
            // console.log(data);

            swal({
                title: 'Success',
                text:"Berhasil Menambahkan Data",
                type:"success",
                timer: 1000,
                showConfirmButton: false
            }, function() {
                form.submit();
            });
        })
    });

    function codeButton(){
        $('#name').val(Math.random().toString(36).substr(2, 5));
    }

     $(document).ready(function () {
        $('#dob').datepicker({
         //merubah format tanggal datepicker ke dd-mm-yyyy
            format: "dd-mm-yyyy",
            //aktifkan kode dibawah untuk melihat perbedaanya, disable baris perintah diatasa
            //format: "dd-mm-yyyy",
            autoclose: true
        });
    });

</script>
@endsection