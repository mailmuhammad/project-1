@extends('layouts.home')

@section('title', 'Admin BSB | Edit Product')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header"></div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Edit Product</h2>
                    </div>
                    <div class="body">
                        <form action="{{ route('products.update', $product->id) }}" method="post" autocomplete="off" class="form-group" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="form-line"><br>
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control" value="{{ $product->name }}">
                            </div>
                            <div class="fallback">
                                <br><label>Upload Photo</label>
                                <input name="image" type="file" multiple value="{{ $product->image }}" />
                            </div>
                            <div class="form-line"><br>
                                <label>Category</label>
                                <select name="product_category_id" id="product_category_id" class="form-control">
                                    <option disabled selected>Pilih Satu</option>
                                    @foreach($categories as $category)
                                        <option {{ $product->product_category_id == $category->id ? "selected" : "" }} value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-line"><br>
                                <label for="desc">Description</label>
                                <textarea id="desc" name="desc" rows="3" class="form-control">{{ $product->desc }}</textarea>    
                            </div>
                            <div class="form-line"><br>
                                <label for="amount">Amount</label>
                                <input type="text" id="amount" name="amount" class="form-control" value="{{ $product->amount }}">
                            </div>
                            <br><button type="submit" class="btn bg-purple">Edit</button>
                            <a href="/products"><button type="submit" class="btn bg-green" style="margin: 15px">Back</button></a>
                        </form>  
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
    

@endsection