@extends('layouts.home')
@section('title', 'Admin BSB | Product')
@section('content')
 
<body class="theme-red">
    
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <button type="button" class="btn bg-purple waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Add New Product</button>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <p align="center">Product</p>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        @if($products->count())
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Category</th>
                                            <th>Description</th>
                                            <th>Amount</th>
                                            <th>Update</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <ol>
                                            @foreach ($products as $p => $product)
                                                <tr>
                                                    <td>{{ $p+1 }}</td>
                                                    <td>{{ $product->name }}</td>
                                                    <td><img src="{{ $product->image }}" class="mask waves-effect waves-light rgba-white-slight" height="100px" alt="gambar"></td>
                                                    <td>{{ $product->produc['name'] }}</td>
                                                    <td>{{ $product->desc }}</td>
                                                    <td>{{ $product->amount }}</td>
                                                    <td><a href="{{ route('products.edit', $product->id) }}"><button type="submit" class="btn bg-purple">Edit</button></a></td>
                                                    <form action="{{ route('products.destroy', $product->id) }}" id="deletedata-{{ $product->id }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <td><button type="submit" class="btn bg-red waves-effect" onclick="deleteNih({{ $product->id }})">Delete</button></td>
                                                    </form>
                                                </tr>   
                                            @endforeach
                                        </ol>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @else
                        	<div class="alert alert-info">You don't have any product</div>
                        @endif
	                    <!-- Modal Dialogs ====================================================================================================================== -->
	                    <!-- Default Size -->
	                    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
	                        <div class="modal-dialog" role="document">
	                            <div class="modal-content">
	                                <div class="modal-header">
	                                    <h4 class="modal-title" id="defaultModalLabel" align="center" >Input Product</h4>
	                                </div>
	                                <div class="body">
	                                    <form action="{{ route('products.store') }}" method="post" id="simpanData" autocomplete="off" class="form-group" enctype="multipart/form-data">
	                                        @csrf
	                                        <div class="form-line"><br>
	                                            <label for="name">Name</label>
	                                            <input type="text" id="name" name="name" class="form-control">
	                                        </div>
                                            <div class="fallback">
                                                <br><label>Upload Photo</label>
                                                <input name="image" type="file" multiple />
                                            </div>
	                                        <div class="form-group"><br>
                                                <label>Category</label>
	                                        	<select name="product_category_id" id="product_category_id" class="form-control show-tick">
	                                        		<option disabled selected>Pilih Satu</option>
	                                        		@foreach($categories as $category)
	                                        			<option value="{{ $category->id }}">{{ $category->name }}</option>
	                                        		@endforeach
	                                        	</select>
	                                        </div>
	                                        <div class="form-line"><br>
	                                            <label for="description">Description</label>
	                                            <textarea id="desc" name="desc" rows="2" class="form-control"></textarea>    
	                                        </div>
	                                        <div class="form-line"><br>
	                                        	<label for="amount">Amount</label>
	                                        	<input type="text" id="amount" name="amount" class="form-control">
	                                        </div>
	                                        
	                                        <br><div class="modal-footer"><br>
	                                            <button type="submit" class="btn btn-link waves-effect">SAVE</button>
	                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
	                                        </div>

	                                    </form>  
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    </div>
                </div>
            </div>
           
        </div>
    </section>
</body>

@endsection

@section('script')

<script type="text/javascript" class="init">
    function deleteNih(id){
        event.preventDefault();
        var form = document.querySelector('#deletedata-'+id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            form.submit();
        })
    }

    $(document).ready(function() {
        $('#simpanData').on('submit',function(e){
            e.preventDefault();
            // console.log('uoioiu');

            var form = document.querySelector('#simpanData');
            var data = new FormData(form);

            swal({
                title: 'Success',
                text:"Berhasil Menambahkan Data",
                type:"success",
                timer: 1000,
                showConfirmButton: false
            }, function() {
                form.submit();
            });
        })
    });

</script>
@endsection