@extends('layouts.home')

@section('title', 'Admin BSB | Edit Member Category')

@section('content')

<section class="content">
<div class="container-fluid">
    <div class="block-header"></div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Member Category</h2>
                </div>
                <div class="body">
                    <form action="{{ route('member_categories.update', $category->id) }}" method="post" autocomplete="off" class="form-group" >
                        @csrf
                        @method('put')
                        <div class="form-line"><br>
                            <label for="name">Nama Kategori</label>
                            <input type="text" id="name" name="name" class="form-control" value="{{ $category->name }}">
                        </div>
                        <br><button type="submit" class="btn bg-purple">Edit</button>
                        <a href="/member_categories"><button type="submit" class="btn bg-green" style="margin: 15px">Back</button></a>
                    </form>  
                </div>
            </div>
            </div>
        </div>
</div>
</div>
</section>
    

@endsection