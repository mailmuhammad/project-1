@extends('layouts.home')

@section('title', 'Admin BSB | Edit Transactions')

@section('content')

<section class="content">
<div class="container-fluid">
    <div class="block-header"></div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Transactions</h2>
                </div>
                <div class="body">
                    <form action="{{ route('transactions.update', $transaction->id) }}" method="post" autocomplete="off" class="form-group" >
                        @csrf
                        @method('put')
                        <div class="form-line"><br>
                            <label for="trx_number">Nomor Transaksi</label>
                            <input disabled type="text" id="trx_number" name="trx_number" class="form-control" value="{{ $transaction->trx_number }}">
                        </div>
                        <div class="form-line"><br>
                            <label>Category</label>
                            <select name="product_id" id="product_id" class="form-control">
                                <option disabled selected>Pilih Satu</option>
                                @foreach($product as $prod) 
                                    <option {{ $transaction->product_id == $prod->id ? "selected" : "" }} value="{{ $prod->id }}">{{ $prod->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-line"><br>
                            <label for="quantity">Jumlah</label>
                            <input id="quantity" name="quantity" class="form-control" value="{{ $transaction->quantity }}">
                        </div>
                        <div class="form-line"><br>
                            <label for="discount">Discount</label>
                            <input id="discount" name="discount" class="form-control" value="{{ $transaction->discount }}">
                        </div>
                        <div class="form-line"><br>
                            <label for="total">Total</label>
                            <input id="total" name="total" class="form-control" value="{{ $transaction->total }}">
                        </div>
                        <br><button type="submit" class="btn bg-purple">Edit</button>
                        <a href="/promos"><button type="submit" class="btn bg-green" style="margin: 15px">Back</button></a>
                    </form>  
                </div>
            </div>
            </div>
        </div>
</div>
</div>
</section>
    

@endsection