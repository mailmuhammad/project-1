<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member_Categories extends Model
{

	use SoftDeletes;

    protected $guarded = ['id'];
    
    protected $table = 'member_categories';

    public function membCategory() {
    	return $this->hasMany(Members::class, 'member_category_id');
    }
}
