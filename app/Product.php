<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name', 'image', 'product_category_id', 'desc', 'amount'];

    public function produc()
    {
    	return $this->belongsTo(Product_Categories::class, 'product_category_id');
    }

    public function prodPromo() {
    	return $this->hasMany(Promo::class, 'product_id');
    }

    public function prodTransaksi() {
    	return $this->hasMany(Transactions::class, 'product_id');
    }

}
