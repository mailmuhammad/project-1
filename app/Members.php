<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Members extends Model
{

	use SoftDeletes;

    protected $guarded = ['id'];

    public function category() {
    	return $this->belongsTo(Member_Categories::class, 'member_category_id');
    }
}
