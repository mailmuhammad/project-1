<?php

namespace App\Http\Controllers;
use App\Product_Categories;
use App\Product;
use Illuminate\Http\Request;

class Product_CategoriesController extends Controller
{
    public function index()
    {
    	$categories = Product_Categories::all();
    	return view('app.product_category.index', compact('categories'));
    }

    public function store(Request $request)
    {

		$request->validate([
			'name' => 'required',
			'desc' => 'required',
		]);

		$categories = new Product_Categories();
        $categories->name = $request->name;
        $categories->desc = $request->desc;
        $categories->save();
		return back();
    }

    public function edit(Product_Categories $category)
    {
    	return view('app.product_category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
    	$request->validate([
            'name' => 'required',
            'desc' => 'required',
        ]);

        $category = Product_Categories::findOrFail($id);
        $category->name = $request->name;
        $category->desc = $request->desc;
        $category->update();

    	return redirect("/categories");
    }

    public function destroy(Product_Categories $category)
    {
    	$category->delete();
    	return back();
    }

    public function show(Product_Categories $category)
    {
        $product = Product::all();
        return view('app.category.showcategory', compact('category', 'product'));
    }

}
