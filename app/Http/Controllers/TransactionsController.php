<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Product;
use App\Transactions;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $transactions = Transactions::all();
         $produc = Product::all();
         return view('app.transactions.index', compact('transactions', 'produc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required',
            'quantity' => 'required',
        ]);


        //trx number generator
        $rmdash = str_replace("-","",Carbon::now()->toDateTimeString());
        $rmcolon = str_replace(":","", $rmdash);
        $rmspace = str_replace(" ","",$rmcolon).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $trx_number = 'TRX'.$rmspace;

        $init = Product::where('id', $request->product_id)->get()->first()->amount;

        $total = $request->quantity * $init;
        $transactions = new Transactions();
        $transactions->trx_number = $trx_number;
        $transactions->product_id = $request->product_id;
        $transactions->quantity = $request->quantity;
        $transactions->discount = 0;
        $transactions->total = $total;
        $transactions->save();
        // return response()->json($transactions);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Transactions $transaction)
    {
        $product = Product::all();
        return view('app.transactions.edit', compact('transaction', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'product_id' => 'required',
            'quantity' => 'required',
            'discount' => 'required',
            'total' => 'required',
        ]);

        $transaction = Transactions::findOrFail($id);
        $transaction->product_id = $request->product_id;
        $transaction->quantity = $request->quantity;
        $transaction->discount = $request->discount;
        $transaction->total = $request->total;
        $transaction->update();
        return redirect('/transactions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transactions $transaction)
    {
        $transaction->delete();
        return back();
    }
}
