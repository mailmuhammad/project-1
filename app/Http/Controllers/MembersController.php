<?php

namespace App\Http\Controllers;

use App\Members;
use App\Member_Categories;
use Illuminate\Http\Request;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $member_categories = Member_Categories::all();
        $members = Members::all();
        return view('app.members.index', compact('members', 'member_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'member_category_id' => 'required',
            'full_name' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);

        //barcode
        $rmdash = str_replace("-","",$request->dob);
        $rmcolon = str_replace(":","", $rmdash);
        $rmspace = str_replace(" ","",$rmcolon).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $barcode = 'BC'.$rmspace;

        $member = new Members();
        $member->member_category_id = $request->member_category_id;
        $member->full_name = $request->full_name;
        $member->dob = $request->dob;
        $member->address = $request->address;
        $member->gender = $request->gender;
        $member->barcode = $barcode;
        $member->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function show(Members $members)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function edit(Members $member)
    {
        $member_categories = Member_Categories::all();
        return view('app.members.edit', compact('member', 'member_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Members $member)
    {
        // return response()->json($request);
        $request->validate([
            'member_category_id' => 'required',
            'full_name' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'gender' => 'required',
        ]);

        // $member = Members::findOrFail($id);
        $member->member_category_id = $request->member_category_id;
        $member->full_name = $request->full_name;
        $member->dob = $request->dob;
        $member->address = $request->address;
        $member->gender = $request->gender;
        $member->update();
        return redirect('members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Members  $members
     * @return \Illuminate\Http\Response
     */
    public function destroy(Members $member)
    {
        $member->delete();
        return back();
    }
}
