<?php

namespace App\Http\Controllers;
use App\Product;
use App\Product_Categories;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $categories = Product_Categories::all();
    	$products = Product::with('produc')->get();
    	return view('app.product.index', compact('products', 'categories'));
 
    }

    public function store(Request $request)
    {
    	request()->validate([
    		'name' => 'required',
    		'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    		'product_category_id' => 'required',
    		'desc' => 'required',
    		'amount' => 'required',
    	]);

        if ($request->has('active')) {
            $active = 1;
        }else {
            $active = 0;
        }

        $fileName = str_replace("=","",base64_encode($request->name.time())) . '.' . request()->image->getClientOriginalExtension();
        if (!$request->image->move(storage_path('app/public/product'), $fileName)) {
            return array('error' => 'Gagal upload foto');
        }else{
            $product = new Product();
            $product->name = $request->name;
            $product->image = "storage/product/".$fileName;
            $product->product_category_id = $request->product_category_id;
            $product->desc = $request->desc;
            $product->amount = $request->amount;
            
            $product->save();
        }

        return back();
    }

    public function edit(Product $product)
    {
        $categories = Product_Categories::all();
        return view('app.product.edit', compact('product', 'categories'));
    }

    public function update(Request $request, $id)
    {

        $productPict = Product::where("id","=",$id)->get()->first()->image;
        if (!$request->image) {
            $request->validate([
                'name' => 'required',
                'product_category_id' => 'required',
                'desc' => 'required',
                'amount' => 'required',
            ]);
        }else {
            $request->validate([
                'name' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'product_category_id' => 'required',
                'desc' => 'required',
                'amount' => 'required',
            ]);
            $fileName = str_replace("=","",base64_encode($request->name.time())) . '.' . request()->image->getClientOriginalExtension();
        }

        if ($request->has('active')) {
            $active = 1;
        } else {
            $active = 0;
        }
        
        $product = Product::findOrFail ($id);
        $product->name = $request->name;
        $product->product_category_id = $request->product_category_id;
        $product->desc = $request->desc;
        $product->amount = $request->amount;
        if ($request->hasFile('image')) {
            if(is_file($product->image)) {
                try{
                    unlink($productPict);
                }catch(\Exception $e){

                }
            }
            $request->image->move(storage_path('app/public/product'), $fileName);
            $product->image = "storage/product/".$fileName;
        }else {
            $product->image = $productPict;
        }
        $product->save();

        return redirect('/products');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if (is_file($product->image)) {
            unlink($product->image);
        }
        $product->delete();
        return back();
    }
}
