<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Categories extends Model
{
	protected $table= 'product_categories';
    protected $fillable= ['name', 'desc'];

    public function prod()
    {
    	return $this->hasMany(Product::class, 'product_category_id');
    }

    // public function hasProduct(){
    // 	return this->belongsTo(Product::class, 'productId', 'id')
    // }
}
