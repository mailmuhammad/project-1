<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $guarded = ['id'];

    public function produc(){
    	return $this->belongsTo(Product::class, 'product_id');
    }
}
